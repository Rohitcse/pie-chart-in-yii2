<?php
$con = mysqli_connect('localhost','root','','ycrud');
use sjaakp\gcharts\BarChart;
?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Date', 'Visits'],
 <?php 
 $query = "SELECT count(ip) AS count, vdate FROM country GROUP BY vdate ORDER BY vdate";
 $exec = mysqli_query($con,$query);
 while($row = mysqli_fetch_array($exec)){
 echo "['".$row['vdate']."',".$row['count']."],";
 }
 ?>
        ]);

        var options = {
          title: 'Date vs. Visits comparison',
          hAxis: {title: 'Date', minValue: 0, maxValue: 15},
          vAxis: {title: 'Visits', minValue: 0, maxValue: 15},
          legend: 'none'
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
  <h3>Scatter Chart</h3>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>
