<?php
use dosamigos\chartjs\ChartJs;
?>
<h3>Bubble Chart</h3>
<?= ChartJs::widget([
    'type' => 'bubble',
    'options' => [
        'height' => 50,
        'width' => 100
    ],

        'data' => [
            'labels' => ["First Dataset"],
            'datasets' => [
                [
                    'label' => "My First dataset",
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    
                    'data' => [
                        ['ID', 'X', 'Y', 'Temperature'],
                        ['',   80,  167,      120],
                        ['',   79,  136,      130],
                        ['',   78,  184,      50],
                        ['',   72,  278,      230],
                        ['',   81,  200,      210],
                        ['',   72,  170,      100],
                        ['',   68,  477,      80]
                    ],
                    
                ],
            ],
                
        ]
  ]);
?>