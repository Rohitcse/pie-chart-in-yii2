<?php
use dosamigos\chartjs\ChartJs;
?>
<h3>Polar Chart</h3>
<?= ChartJs::widget([
    'type' => 'polarArea',
    'options' => [
        'height' => 100,
        'width' => 300
    ],

        'data' => [
            'labels' => ["Africa", "Asia", "Europe", "Latin America", "North America"],
            'datasets' => [
                [
                    'label' => "My First dataset",
                    'backgroundColor'=> ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        'data' => [2478,5267,734,784,433]
                ],
               
            ]
        ]
  ]);
?>