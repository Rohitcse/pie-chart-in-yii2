<?php
use dosamigos\chartjs\ChartJs;
?>
<h3>Radar Chart</h3>
<?= ChartJs::widget([
    'type' => 'radar',
    'options' => [
        'height' => 100,
        'width' => 300
    ],

        'data' => [
            'labels' => ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
            'datasets' => [
                [
                    'label' => "My First dataset",
                    'backgroundColor' => 'rgba(255, 99, 132, 0.2)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'pointBackgroundColor' => 'rgb(255, 99, 132)',
                    'pointBorderColor' => '#fff',
                    'pointHoverBackgroundColor' => "#fff",
                    'pointHoverBorderColor' => '#fff',
                    'data' => [65, 59, 90, 81, 56, 55, 40]
                ],
                [
                    'label' => "My Second dataset",
                    'backgroundColor' => 'rgba(54, 162, 235, 0.2)',
                    'borderColor' => 'rgb(54, 162, 235)',
                    'pointBackgroundColor' => 'rgb(54, 162, 235)',
                    'pointBorderColor' => "#fff",
                    'pointHoverBackgroundColor' => "#fff",
                    'pointHoverBorderColor' => 'rgb(54, 162, 235)',
                    'data' => [28, 48, 40, 19, 96, 27, 100]
                ]
            ]
        ]
  ]);
?>