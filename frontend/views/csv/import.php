<?php 
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
?>
<style>  
body  
{  
background-image:url("https://images.pexels.com/photos/6177677/pexels-photo-6177677.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940.jpg"); 
 background-size: cover;
}  
</style>  
<h1>Import Data</h1>
<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>

<?= $form->field($modelImport,'fileImport')->fileInput() ?>

<?= Html::submitButton('Import',['class'=>'btn btn-primary']);?>

<?php ActiveForm::end();?>
<br>
<?= Html::a('Export Excel', ['export-excel'], ['class'=>'btn btn-info']); ?> 