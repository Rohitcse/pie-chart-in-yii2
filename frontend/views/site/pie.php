<?php
use sjaakp\gcharts\PieChart;

?>
    ...
<?= PieChart::widget([
    'height' => '400px',
    'dataProvider' => $dataProvider,
    'columns' => [
        'columns' => [
            'name:string',  // first column: domain
            'population'    // second column: data
        ],
    ],
    'options' => [
        'title' => 'EU Population',
        'sliceVisibilityThreshold' => 0.022
    ],
]) ?>
    ...