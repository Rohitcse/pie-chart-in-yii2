<?php

namespace frontend\controllers;
use Yii;
//namespace frontend\models\Country;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use frontend\models\Country;

class ActiveController extends Controller    {

public function actionPie()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('pie', [
		'dataProvider' => $dataProvider]);
  }

public function actionBar()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('bar', [
		'dataProvider' => $dataProvider]);
   }

   public function actionColumn()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('column', [
		'dataProvider' => $dataProvider]);
   }

   public function actionGeo()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('geo', [
		'dataProvider' => $dataProvider]);
   }

   public function actionScatter()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('scatter', [
		'dataProvider' => $dataProvider]);
   }

   public function actionLine()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('line', [
		'dataProvider' => $dataProvider]);
   }

   public function actionRadar()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('radar', [
		'dataProvider' => $dataProvider]);
   }

   public function actionPolar()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('polar', [
		'dataProvider' => $dataProvider]);
   }

   public function actionBubble()	{
	$dataProvider = new ActiveDataProvider([
		'query' => Country::find(),
	    'pagination' => false
	]);
	
	return $this->render('bubble', [
		'dataProvider' => $dataProvider]);
   }
  
}

?>
