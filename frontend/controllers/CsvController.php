<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Csv;
use yii\widgets\ActiveForm;  
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use PHPExcel_IOFactory;
use yii\base\DynamicModel;


class CsvController extends Controller
{
    public function actionImport()
    {
        $modelImport = new \yii\base\DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'],'required');
        //$modelImport->addRule(['fileImport'],'file',['extensions'=>'csv,ods,xls,xlsx'],['maxSize'=>1024*1024]);

        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            if($modelImport->fileImport && $modelImport->validate()){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);  
                $baseRow = 1;
                $TruncateSql = $this->TruncateData();
		        $TruncateTable = Yii::$app->db->createCommand("$TruncateSql")->execute();
                while(!empty($sheetData[$baseRow]['B'])){
                    $model =new Csv;
                    $model->firstname = (string)$sheetData[$baseRow]['A'];
                    $model->lastname = (string)$sheetData[$baseRow]['B'];
                    $model->email = (string)$sheetData[$baseRow]['C'];     
                    if($model->validate()){   
                        $model->save();  
                    
                    } else{
                         print_r($model->getErrors());
                    }
                    
                    $baseRow++;
                }
                Yii::$app->getSession()->setFlash('success','Success');
            }else{
                Yii::$app->getSession()->setFlash('error','Error');
            }
        }

        return $this->render('import',[
                'modelImport' => $modelImport,
            ]);
    }

   
    public function actionExportExcel()
    {
        
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export').'/templates/phpexcel/export.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
        $baseRow=2; // line 2
        foreach($dataProvider->getModels() as $csv){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, $baseRow-1);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$baseRow, $csv->id);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$baseRow, $csv->firstname);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$baseRow, $csv->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$baseRow, $csv->email);
            $baseRow++;
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="export.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('php://output');
        exit;
    }   

}
