<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Test;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

class TestController extends Controller  {

public function actionImport() {
        
           $model=new Test;

           if(isset($_POST['Test']))
             {

               $model->attributes=$_POST['Test'];

               if($model->validate())
                 {

                  $csvFile=UploadedFile::getInstance($model,'file');  
                  $tempLoc=$csvFile->getTempName();

                    $sql="LOAD DATA LOCAL INFILE '".$tempLoc."'
        INTO TABLE `tbl_user`
        FIELDS
            TERMINATED BY ','
            ENCLOSED BY '\"'
        LINES
            TERMINATED BY '\n'
         IGNORE 1 LINES
        (`firstname`, `lastname`, `email`)
        ";

                    $connection=Yii::app()->db;
                    $transaction=$connection->beginTransaction();
                        try
                            {

                                $connection->createCommand($sql)->execute();
                                $transaction->commit();
                            }
                            catch(Exception $e) // an exception is raised if a query fails
                             {
                                print_r($e);
                                exit;
                                $transaction->rollBack();

                             }
                      //$this->redirect(array("user/index"));


                 }
             }  

           return $this->render('importcsv',array('model'=>$model));

   }
}  