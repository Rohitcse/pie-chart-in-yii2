<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "csv".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 */
class Csv extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'csv';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email'], 'required'],
            [['firstname', 'lastname', 'email'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
        ];
    }
}
