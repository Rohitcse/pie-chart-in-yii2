<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property int $ip
 * @property string $browser
 * @property string $country
 * @property string|null $vdate
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'browser', 'country'], 'required'],
            [['ip'], 'integer'],
            [['vdate'], 'safe'],
            [['browser', 'country'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'browser' => 'Browser',
            'country' => 'Country',
            'vdate' => 'Vdate',
        ];
    }
}
